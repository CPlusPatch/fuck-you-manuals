import pdfkit
import requests
import requests_cache
from bs4 import BeautifulSoup

requests_cache.install_cache('book_cache')
rootUrl = "https://storage.libmanuels.fr/Magnard/manuel/9782210113374/6/OEBPS/"

def fetchIndex():
	print("[-] Recuperation des URLs...")
	htmlText = requests.get(rootUrl + "nav.xhtml").text
	soup = BeautifulSoup(htmlText, 'html.parser')
	linksHtml = soup.find_all("nav", {
		"epub:type": "toc"
	})
	olSoup =  BeautifulSoup(str(linksHtml), 'html.parser').find_all("ol")
	linksSoup = BeautifulSoup(str(olSoup), 'html.parser')
	links = []
	for anchor in linksSoup.find_all("a", href = True):
		links.append(anchor["href"])
	
	return links

def downloadPage(url, id):
	print(rootUrl + url)
	pdfkit.from_url(rootUrl + url, f"output/{id}.pdf", {
		"margin-right": "0",
		"margin-left": "0",
		"margin-top": "0",
		"margin-bottom": "0",
	})

def downloadAll(index: list):
	print(index)
	for url in index:
		downloadPage(url, index.index(url))