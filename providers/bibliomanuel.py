import os

import pdfkit
import requests
import requests_cache
from bs4 import BeautifulSoup

requests_cache.install_cache('book_cache')
rootUrl = "https://biblio.manuel-numerique.com/epubs/BORDAS/bibliomanuels/distrib_gp/1/9/8731/online/OEBPS/"

def fetchIndex():
	print("[-] Recuperation des URLs...")
	htmlText = requests.get(rootUrl + "TOC.xhtml").text
	soup = BeautifulSoup(htmlText, 'html.parser')
	linksHtml = soup.find_all("nav", {
		"epub:type": "page-list"
	})
	linksSoup =  BeautifulSoup(str(linksHtml), 'html.parser')
	links = []
	for anchor in linksSoup.find_all("a", href = True):
		links.append(anchor["href"])
	
	print(f"Total pages: {len(links)}")

	return links

def downloadPage(url, id):
	if url == "":
		return
	pdfkit.from_url(rootUrl + list(url.split("#"))[0], f"output/{id}.pdf", {
		"margin-right": "0",
		"margin-left": "0",
		"margin-top": "0",
		"margin-bottom": "0",
	})

def resumeTransferIfHalted(index):
	if os.path.exists(os.path.join(os.path.dirname(__name__), "output/0.pdf")):
		if input("Un transfert en cours a ete detecte: voulez vous continuer? (y/N)") in ("y", "Y", "YES", "yes"):
			furthestFileDownloaded = 0
			for i in range(0, len(index)):
				if os.path.exists(os.path.join(os.path.dirname(__name__), f"output/{i}.pdf")):
					furthestFileDownloaded = i
			return ([""] * (furthestFileDownloaded + 1)) + index[(furthestFileDownloaded + 1):]
	return index

def downloadAll(index: list):
	for url in index:
		if url != "":
			print(f"Telechargement... {round(index.index(url) / len(url), 2)}% ({index.index(url) + 1}/{len(index)})")
		downloadPage(url, index.index(url))