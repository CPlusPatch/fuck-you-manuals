from providers import bibliomanuel, libmanuels

provider = input("De quel site voulez-vous telecharger?\n(bilbiomanuels (bm), libmanuels (lm)): ")
if provider in ("libmanuels", "lm"):
	index = libmanuels.fetchIndex()
	libmanuels.downloadAll(index)
elif provider in ("bibliomanuels", "bm"):
	index = bibliomanuel.fetchIndex()
	index = bibliomanuel.resumeTransferIfHalted(index) 
	print(index)
	bibliomanuel.downloadAll(index)
